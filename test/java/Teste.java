import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

public class Teste {
	
	private Spaceship spaceship;
	private Explosion explosion;
    private ArrayList<Enemies> enemy;
    private ArrayList<Bullet> bullet;
	
	@Before 
	public void setUp() {
		spaceship = new Spaceship(Game.getWidth()/2, Game.getHeight()/2);
		explosion = new Explosion(Game.getWidth()/2, Game.getHeight()/2);
		enemy = new ArrayList<Enemies>();
		bullet = new ArrayList<Bullet>();
		
		//map.update();
		enemy.add(new Enemies((int) Game.getWidth()/2, Game.getHeight()/2, 1));
		bullet.add(new Bullet(spaceship.getX(), spaceship.getY()));
		
	}

	@Test
	public void testVisible() {
		
		assertTrue(spaceship.isVisible()); // verifica se a visibilidade da nave é true após sua criação
		assertTrue(explosion.isVisible()); // verifica se a visibilidade da explosão é true após sua criação
		assertTrue(enemy.get(0).isVisible()); // verifica se a visibilidade do inimigo é true após sua criação
		assertTrue(bullet.get(0).isVisible()); // verifica se a visibilidade da bala da nave é true após sua criação
		
	}
	
	@Test
	public void testColision() {
		Colision.hit(spaceship, bullet, enemy);
		
		assertEquals(2, spaceship.getLife()); // verifica se a nave perdeu uma vida após a colisão com uma nave inimiga
		assertFalse(enemy.get(0).isVisible()); // verifica se a visibilidade da nave inimiga é false após a colisão
		assertFalse(bullet.get(0).isVisible()); // verifica se a visibilide da bala da nave é false após a colisão
		
	}


}
