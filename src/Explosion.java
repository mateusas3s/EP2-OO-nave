public class Explosion extends Sprite {

	public Explosion(int x, int y) {
        super(x, y);  
        
        loadImage("images/explosion.png");
    }
	
}