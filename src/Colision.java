import java.util.ArrayList;
import java.util.List;

public class Colision {
	
	public static void hit(Spaceship s, List<Bullet> b, ArrayList<Enemies> e){
		for (int i = 0; i < b.size(); i++) {
			for (int j = 0; j < e.size(); j++){
				if(b.get(i).getBounds().intersects(e.get(j).getBounds()) || e.get(j).getBounds().intersects(b.get(i).getBounds())){
					e.get(j).setVisible(false);
					b.get(i).setVisible(false);
					s.setPontuation(e.get(j).getBonus());
					
				}
			}
		}
		
		for (int i = 0; i < e.size(); i++) {
			if (s.getBounds().intersects(e.get(i).getBounds()) || e.get(i).getBounds().intersects(s.getBounds())) {
				s.setLife();
				e.get(i).setVisible(false);
				
			}
		}
		
	}
	
}
