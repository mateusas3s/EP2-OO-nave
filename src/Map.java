import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.Timer;

public class Map extends JPanel implements ActionListener {

    private final int SPACESHIP_X = 230;
    private final int SPACESHIP_Y = 530;
    private final Timer timer_map;
    
    private final Image background;
    private final Spaceship spaceship;
    private Explosion explosion;
    private ArrayList<Enemies> enemy;
    private int wave = 0;
    private boolean exploud = false;
    
    
    public Map() {
        
        addKeyListener(new KeyListerner());
        
        setFocusable(true);
        setDoubleBuffered(true);
        ImageIcon image = new ImageIcon("images/space.jpg");
        
        this.background = image.getImage();

        spaceship = new Spaceship(SPACESHIP_X, SPACESHIP_Y);
        
        initEnemy();
        
        timer_map = new Timer(Game.getDelay(), this);
        timer_map.start();
                            
    }
    
    public void initEnemy(){
    	
    	enemy = new ArrayList<Enemies>();
    	
    	Random rand = new Random();
    	
    	int numEnemies[][] = {{5,0,0},{10,0,0},{5,5,0},{10,5,0},
    						  {5,5,5},{10,5,5},{5,10,5},{10,10,5},
    						  {5,5,10},{10,5,10},{5,10,10},{10,10,10},
    						  {15,10,10},{20,10,10},{15,15,10},{20,15,10},
    						  {15,15,15},{20,15,15},{15,20,15},{20,20,15},
    						  {15,15,20},{20,15,20},{15,20,20},{20,20,20}};  
        
    		for(int i = 0; i < numEnemies[wave][0]; i++)
    			enemy.add(new Enemies((int) (Math.random()*(Game.getWidth()/(rand.nextInt(3)+1))), -50, 1));

    		for(int i = numEnemies[wave][0]; i < numEnemies[wave][0]+numEnemies[wave][1]; i++)
    			enemy.add(new Enemies((int) (Math.random()*(Game.getWidth()/(rand.nextDouble()+1))), -50, 2));
        
    		for(int i = numEnemies[wave][0]+numEnemies[wave][1]; i < numEnemies[wave][0]+numEnemies[wave][1]+numEnemies[wave][2]; i++)
    			enemy.add(new Enemies((int) (Math.random()*(Game.getWidth()/(rand.nextInt(3)+1))), -50, 3));
    }
    
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        g.drawImage(this.background, 0, 0, null);
       
        draw(g);
        
        if(spaceship.getLife() >= 0)
        	drawLife(g);
        
        if(spaceship.getLife()<=0)
        	drawGameOver(g);
        
        if(wave >= 20)
        	drawMissionCompleted(g);
        
        Toolkit.getDefaultToolkit().sync();
    }

    private void draw(Graphics g) {
    	
    	//Draw Explosion
    	if(exploud){
    		g.drawImage(explosion.getImage(), explosion.getX(), explosion.getY(), this);
    	}
    	// Draw bullets
    	List<Bullet> bullets = spaceship.getBullet();

		for (int i = 0; i < bullets.size(); i++) {
			Bullet b = (Bullet) bullets.get(i);
			g.drawImage(b.getImage(), b.getX(), b.getY(), this);
		}

        // Draw spaceship
		if(spaceship.getLife() > 0)
			g.drawImage(spaceship.getImage(), spaceship.getX(), spaceship.getY(), this);
		
    	// Draw enemy
    	for(int i = 0; i < enemy.size(); i++)
        	g.drawImage(enemy.get(i).getImage(), enemy.get(i).getX(), enemy.get(i).getY(), this);
    	
    	drawWave(g);
        
        drawPoints(g); 
        	
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
       
        update();
        repaint();
    }
    
    private void drawLife(Graphics g) {

        String message = "LIFE: " + spaceship.getLife();
        Font font = new Font("Helvetica", Font.BOLD, 12);
        g.setColor(Color.white);
        g.setFont(font);
        g.drawString(message, 10, 20);
    }
    
    private void drawWave(Graphics g) {

        String message = "WAVE " + (wave + 1);
        Font font = new Font("Helvetica", Font.BOLD, 12);
        FontMetrics metric = getFontMetrics(font);
        g.setColor(Color.white);
        g.setFont(font);
        g.drawString(message, Game.getWidth() - metric.stringWidth(message) - 10, 20);
    }
    
    private void drawPoints(Graphics g) {

        String message = "POINTS: " + spaceship.getPontuation();
        Font font = new Font("Helvetica", Font.BOLD, 12);

        g.setColor(Color.white);
        g.setFont(font);
        g.drawString(message, 10, 40);
    }
    
    private void drawGameOver(Graphics g) {

        String message = "GAME OVER";
        Font font = new Font("Helvetica", Font.BOLD, 14);
        FontMetrics metric = getFontMetrics(font);

        g.setColor(Color.white);
        g.setFont(font);
        g.drawString(message, (Game.getWidth() - metric.stringWidth(message)) / 2, Game.getHeight() / 2);
    }
    
    private void drawMissionCompleted(Graphics g) {

        String message = "Mission Completed";
        Font font = new Font("Helvetica", Font.BOLD, 14);
        FontMetrics metric = getFontMetrics(font);

        g.setColor(Color.white);
        g.setFont(font);
        g.drawString(message, (Game.getWidth() - metric.stringWidth(message)) / 2, Game.getHeight() / 2);
    }
    
    public void update() {
    	
    	//update bullets
    	List<Bullet> bullets = spaceship.getBullet();

		for (int i = 0; i < bullets.size(); i++) {
			if(bullets.get(i).isVisible()){
				bullets.get(i).move();
			}else{
				bullets.remove(i);
			}
		}
		//update Enemy
		if(enemy.isEmpty()){
			wave++;
			initEnemy();
			
		}
        
        for(int i = 0; i < enemy.size(); i++){
        	if(enemy.get(i).isVisible()){
        		enemy.get(i).move();
        		exploud = false;
        	}else{
        		explosion = new Explosion(enemy.get(i).getX(), enemy.get(i).getY());
        		exploud = true;
        		enemy.remove(i);
        	}
        }
        
        //update Spaceship
        if(spaceship.getLife() > 0 & wave < 20)
        	spaceship.move();
        
        //checking colision
        Colision.hit(spaceship, bullets, enemy);
    }
  

    private class KeyListerner extends KeyAdapter {
    	
    	
        @Override
        public void keyPressed(KeyEvent e) {
        	if(spaceship.getLife() > 0 & wave < 20)
        		spaceship.keyPressed(e);
        }

        @Override
        public void keyReleased(KeyEvent e) {
        	if(spaceship.getLife() > 0 & wave < 20)
        		spaceship.keyReleased(e);
        	
        }
    }
}