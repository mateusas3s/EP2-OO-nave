public class Bullet extends Sprite {
	
	private int speed = -15;

	public Bullet(int x, int y) {
        super(x, y);  
        
        loadImage("images/missile.png");
    }
	
	 public void move() {
		 if(this.y < 0){
			 this.visible = false;
		 }
		 y += speed;
	 }
}
