

public class Enemies extends Sprite{
	
	private int speed_x;
    private int speed_y;
    private int bonus;

	public Enemies(int x, int y, int type) {
        super(x, y);

        if(type == 1){
        	loadImage("images/alien_EASY.png");
        	speed_x = 2;
        	speed_y = 2;
        	bonus = 1;
        }
        if(type == 2){
        	loadImage("images/alien_MEDIUM.png");
        	speed_x = 3;
        	speed_y = 3;
        	bonus = 5;
        }
        if(type == 3){
        	loadImage("images/alien_HARD.png");
        	speed_x = 4;
        	speed_y = 4;
        	bonus = 10;
        }
                
}
	
public void move() {
	
		double angle = Math.random()*180;
        int rad = (int) Math.toRadians(angle);
        // Limits the movement of the spaceship to the side edges.
        if((speed_x < 0 && x <= 5) || (speed_x > 0 && x + width + 11 >= Game.getWidth())){
            speed_x = -speed_x;
        }
         
        // Moves the spaceship on the horizontal axis
        x += Math.cos(rad)*speed_x;
        
        // Limits the movement of the spaceship to the vertical edges.
        if((speed_y < 0 && y <= 5) || (speed_y > 0 && y + height + 25 >= Game.getHeight())){
            speed_y = -speed_y;
        }

        // Moves the spaceship on the verical axis
        y += Math.sin(rad)*speed_y;
        
}

public int getBonus() {
	return bonus;
}

public void setBonus(int bonus) {
	this.bonus = bonus;
}

}
