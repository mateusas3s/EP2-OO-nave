import java.awt.Color;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;


public class Application extends JFrame {
	
	private JFrame menu;
	private JLabel headerLabel;
	private JPanel controlPanel;
	private Map map;
	
    public Application() {
    	construirMenu();
    	
    }
	
	private void construirMenu() {
		menu = new JFrame("Space Combat Game");
		menu.setLocationRelativeTo(null);
		menu.setSize(300, 200);
		menu.setResizable(true);
		menu.setBackground(Color.WHITE);
		menu.setLayout(new GridLayout(2, 1));
		
		headerLabel = new JLabel("",JLabel.CENTER );       

	    menu.addWindowListener(new WindowAdapter() {
	    	public void windowClosing(WindowEvent windowEvent){
	    		System.exit(0);
	        }        
	    });    
	    
	    controlPanel = new JPanel();
	    controlPanel.setLayout(new FlowLayout());

	    menu.add(headerLabel);
	    menu.add(controlPanel);
	    menu.setVisible(true);
	    mostraMenu();
	}
	
	private void mostraMenu(){
		headerLabel.setText("MENU"); 
		
		JButton playButton = new JButton("Play");
	    JButton aboutButton = new JButton("About");
	    JButton quitButton = new JButton("Quit");
	    
	    playButton.addActionListener(new ActionListener(){
	    	@Override
	    	public void actionPerformed(ActionEvent e){
	    		construirFase();
	    		menu.setVisible(false);
	    	}
	    }); 

	    aboutButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane
						.showMessageDialog(
								null,
								"Desenvolvido por Mateus Augusto Sousa e Silva \nMatrícula: 15/0062869 \n\nVersão 1.0 - 2017",
								"Information", JOptionPane.INFORMATION_MESSAGE);
			}
		}); 
	    
	    quitButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		
	    controlPanel.add(playButton);
	    controlPanel.add(aboutButton);
	    controlPanel.add(quitButton);       

	}
	
	private void construirFase() {
		map = new Map();
		add(map);
		
		construirMenuBar();
		setSize(Game.getWidth(), Game.getHeight());
        setName("Space Combat Game");
        setResizable(true);
        setLocationRelativeTo(null);
        setVisible(true);
        
	}
	
	private void construirMenuBar() {
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBackground(Color.WHITE);
		menuBar.setBorder(new LineBorder(Color.red));
		JMenuItem voltar = new JMenuItem("Voltar");
		voltar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				map.setVisible(false);
				construirMenu();

			}
		});
		menuBar.add(voltar);
		setJMenuBar(menuBar);

	}
	
    public static void main(String[] args) {
        
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Application();
                
            }
        });
    }
}