Mateus Augusto Sousa e Silva

15/0062869

JOGO:
Em Space Combat Game você é um pilot espacial que tem como objetivo proteger a zona espacial da Terra, contra uma invasão alienígenas do palneta Tremiturium.
O objetivo do Jogo é destruir todos os aliens tremitorianos que tentarem atacar a Terra. Existem 3 tipos de naves alien, cada uma com um nível de velocidade (tremedeira). Estes Aliens organizam suas investidas e avançam para a Terra em ondas. Se você destruir todos os aliens das 20 ondas de ataque, a Terra estará a salvo.
O escudo defletor da nave que você pilota suporta até 2 impactos. No terceiro, a nave é destruida. 

COMANDOS: 
-Movimentar com setinhas
-Atirar com a tecla Espaço
